package com.example.sindre.lab4;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class SettingsActivity extends AppCompatActivity{
    public Button applyButton;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        Spinner refreshSpinner = findViewById(R.id.refreshSpinner);
        TextView usernameView = findViewById(R.id.usernameView);

        List<String> refresh = new ArrayList<>();
        refresh.add("10 min");
        refresh.add("1 hour");
        refresh.add("24 hours");

        ArrayAdapter<String> refreshAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, refresh);
        refreshAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        refreshSpinner.setAdapter(refreshAdapter);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        refreshSpinner.setSelection(prefs.getInt("refresh", 1));

        String username = prefs.getString("username", null);
        usernameView.setText(username);

        applyButton = findViewById(R.id.applyButton);
        applyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
                SharedPreferences.Editor editor = prefs.edit();
                Spinner refreshSpinner = findViewById(R.id.refreshSpinner);
                editor.putInt("refresh", refreshSpinner.getSelectedItemPosition());
                editor.apply();

                finish();
            }
        });
    }
}
