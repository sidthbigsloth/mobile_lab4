package com.example.sindre.lab4;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class FriendsFragment extends android.support.v4.app.Fragment implements SwipeRefreshLayout.OnRefreshListener
{
    ListView usernameView;
    ArrayList<String> usernames;

    public static final String USERNAME = "usernameChosen";

    public SwipeRefreshLayout refreshLayout;

    private FirebaseFirestore db;

    public FriendsFragment()
    {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.fragment_friends, container, false);

        usernameView = rootView.findViewById(R.id.list);
        db = FirebaseFirestore.getInstance();

        refreshFriends();

        usernameView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int pos, long id)
            {
                String username = usernames.get(pos);
                Intent intent = new Intent(getActivity(), FriendActivity.class);
                intent.putExtra(USERNAME, username);
                startActivity(intent);
            }
        });

        refreshLayout = rootView.findViewById(R.id.swipeRefreshLayout);
        refreshLayout.setOnRefreshListener(this);

        return rootView;
    }

    public void refreshFriends()
    {   //gets users
        db.collection("users")
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(QuerySnapshot friends, FirebaseFirestoreException e) {
                        if(e != null)
                        {
                            Log.d("FriendsFragment", "listen failed", e);
                            return;
                        }

                        usernames = new ArrayList<>();
                        for(DocumentSnapshot doc : friends)
                        {
                            if(doc.get("username") != null)
                            {
                                usernames.add(doc.getString("username"));
                            }
                        }

                        Collections.sort(usernames, new Comparator<String>() {
                            @Override
                            public int compare(String s, String t1) {
                                return s.compareToIgnoreCase(t1);
                            }
                        });

                        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, usernames);
                        usernameView.setAdapter(adapter);

                        if(usernames == null)
                        {
                            Toast.makeText(getActivity(), "No friends :(", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    @Override
    public void onRefresh()
    {
        refreshLayout.setColorScheme(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        refreshFriends();
        refreshLayout.setRefreshing(false);
    }
}
