package com.example.sindre.lab4;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ListView;

import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;


public class FriendActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener
{
    private ListView messagesView;
    private FirebaseFirestore db;
    private String username;
    private ArrayList<Message> messages;

    public SwipeRefreshLayout refreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_friends);

        Intent intent = getIntent();
        username = intent.getStringExtra(FriendsFragment.USERNAME);

        db = FirebaseFirestore.getInstance();
        messagesView = findViewById(R.id.list);
        messages = getMessages();

        refreshLayout = findViewById(R.id.swipeRefreshLayout);
        refreshLayout.setOnRefreshListener(this);
    }

    private ArrayList<Message> getMessages()
    {   //gets all messages
        final ArrayList<Message> messages = new ArrayList<>();

        db.collection("messages").addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(QuerySnapshot input, FirebaseFirestoreException e) {
                if(e != null)
                {
                    Log.d("FriendActivity", "Listen failed");
                    return;
                }

                for(DocumentSnapshot doc : input)
                {
                    String dbUser = doc.get("user").toString();
                    if(dbUser.equals(username))
                    {
                        Message message = new Message();
                        message.setUser(doc.get("user").toString());
                        message.setMessage(doc.get("message").toString());
                        message.setDate(doc.get("date").toString());
                        messages.add(message);
                    }
                }

                //sorts messages
                Collections.sort(messages, new Comparator<Message>() {
                    @Override
                    public int compare(Message message, Message t1) {
                        if(message.getDate() == null || t1.getDate() == null)
                            return 0;
                        return t1.getDate().compareTo(message.getDate());
                    }
                });

                messagesView.setAdapter(new MyAdapter(FriendActivity.this, messages));
            }
        });
        return messages;
    }

    @Override
    public void onRefresh()
    {
        refreshLayout.setColorScheme(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        messages = getMessages();
        refreshLayout.setRefreshing(false);
    }
}
