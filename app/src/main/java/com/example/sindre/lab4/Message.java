package com.example.sindre.lab4;

public class Message
{
    private String date = "";
    private String user = "";
    private String message = "";

    void setDate(String d)
    {
        date = d;
    }
    void setUser(String u)
    {
        user = u;
    }
    public void setMessage(String m)
    {
        message = m;
    }

    String getDate()
    {
        return date;
    }
    String getUser()
    {
        return user;
    }
    public String getMessage() {return message;}
}
