package com.example.sindre.lab4;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

public class MainActivity extends AppCompatActivity {
    final private int NAME_MAX = 35;
    private static final String TAG = "MainActivity";

    private String userName;
    private ArrayList<String> names = new ArrayList<>();

    private FirebaseFirestore db;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        PageAdapter pageAdapter;
        ViewPager mViewPager;

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        pageAdapter = new PageAdapter(getSupportFragmentManager());
        pageAdapter.addFrag(new MessagesFragment(), "Messages");
        pageAdapter.addFrag(new FriendsFragment(), "Friends");

        mViewPager = findViewById(R.id.container);
        mViewPager.setAdapter(pageAdapter);

        EditText text = findViewById(R.id.messageTxt);
        Button button = findViewById(R.id.send);

        text.setVisibility(View.GONE);
        button.setVisibility(View.GONE);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText text = findViewById(R.id.messageTxt);
                Button button = findViewById(R.id.send);
                if(text.getVisibility() == View.GONE)
                {
                    text.setVisibility(View.VISIBLE);
                    button.setVisibility(View.VISIBLE);
                }
                else
                {
                    text.setVisibility(View.GONE);
                    button.setVisibility(View.GONE);
                }
            }
        });

        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();

        setUserName();
    }

    @Override
    public void onStart()
    {
        super.onStart();

        final FirebaseUser currentUser = mAuth.getCurrentUser();

        if(currentUser == null && mAuth != null)
        {
            mAuth.signInAnonymously()
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if(task.isSuccessful())
                            {
                                Log.d(TAG, "Signed in anonymously");
                                Toast.makeText(MainActivity.this, "You are anonymous", Toast.LENGTH_SHORT).show();
                            }
                            else
                            {
                                Log.d(TAG, "Failed to sign in anonymously");
                                Toast.makeText(MainActivity.this, "Failed to sign you in anonymously, check connection", Toast.LENGTH_LONG).show();
                            }
                        }
                    });
        }
        else if(mAuth == null)
            Log.d(TAG, "mAuth is null?");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here
        int id = item.getItemId();


        if (id == R.id.action_settings) {
            startActivity(new Intent(MainActivity.this, SettingsActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setUserName()
    {   //prompts user to enter a username if there isn't one
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        userName = prefs.getString("username", null);

        if(userName == null || userName.isEmpty())
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Enter username");

            final EditText userNameTxt = new EditText(this);
            userNameTxt.setInputType(InputType.TYPE_CLASS_TEXT);
            userNameTxt.setHint("username");
            userNameTxt.setText(randomUsername());
            builder.setView(userNameTxt);

            builder.setPositiveButton("OK", new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialogInterface, int i)
                {
                    String name = userNameTxt.getText().toString();
                    if (name.isEmpty() || name.length() > NAME_MAX)
                    {
                        Toast.makeText(MainActivity.this, "Username must be between 1 and " + NAME_MAX + " characters", Toast.LENGTH_LONG).show();
                        setUserName();
                    }
                    else if(!isUnique(name))
                    {
                        Toast.makeText(MainActivity.this, "Username already taken, You are provided with a random unique one", Toast.LENGTH_LONG).show();
                        setUserName();
                    }
                    else
                    {
                        userName = name;

                        Map<String, Object> toStore = new HashMap<>();
                        toStore.put("username", userName);

                        db.collection("users")
                                .add(toStore)
                                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                                    @Override
                                    public void onSuccess(DocumentReference documentReference) {
                                        Log.d(TAG, "User added with id: " + documentReference.getId());
                                        SharedPreferences.Editor editor = prefs.edit();
                                        editor.putString("username", userName);
                                        editor.apply();
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Log.d(TAG, "Error adding user", e);
                                    }
                                });

                    }
                }
            });

            builder.show();
        }
    }

    private String randomUsername()
    {   //makes a random username
        String username;
        do
        {
            username = randomString();
        }while (!isUnique(username));
        return username;
    }

    private String randomString()
    {   //generates a random string
        Random gen = new Random();
        StringBuilder stringBuilder = new StringBuilder();
        int length = gen.nextInt(NAME_MAX-1)+1;
        char temp;
        for(int i = 0; i < length; i++)
        {
            temp = (char) (gen.nextInt(94) + 33);
            stringBuilder.append(temp);
        }
        return stringBuilder.toString();
    }

    private boolean isUnique(final String name)
    {   //checks if a given username is unique, returns true if it is
        boolean unique = true;

        db.collection("users").addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(QuerySnapshot users, FirebaseFirestoreException e) {
                if (e != null)
                {
                    Log.d("MainActivity", "Listener failed", e);
                    return;
                } else
                    Log.d("MainActivity", "Listener success");

                names.clear();
                for (DocumentSnapshot doc : users)
                {
                    if (doc.get("username") != null)
                    {
                        names.add(doc.get("username").toString());
                    }
                }
            }
        });

        for(String n : names)
        {
            if (name.equals(n))
                unique = false;
            else if(!unique)
                break;
        }

        return unique;
    }

    public void onClick(View view)
    {   //sends a message if it's not too big
        EditText text = findViewById(R.id.messageTxt);
        String m = text.getText().toString();
        if(!m.isEmpty())
        {
            if(m.length() > 256)
                Toast.makeText(MainActivity.this, "Message too long", Toast.LENGTH_LONG).show();
            else
            {
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
                String username = prefs.getString("username", null);

                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.UK);
                String date = dateFormat.format(Calendar.getInstance().getTime());

                Map<String, Object> data = new HashMap<>();
                data.put("date", date);
                data.put("user", username);
                data.put("message", m);

                db.collection("messages")
                        .add(data)
                        .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                            @Override
                            public void onSuccess(DocumentReference documentReference) {
                                Log.d("MainActivity", "Message sent with id: " + documentReference.getId());
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.d("MainActivity", "Message not sent");
                            }
                        });

                text.getText().clear();
                Button button = findViewById(R.id.send);
                text.setVisibility(View.GONE);
                button.setVisibility(View.GONE);
            }
        }
    }
}
