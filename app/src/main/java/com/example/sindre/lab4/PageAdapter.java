package com.example.sindre.lab4;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class PageAdapter extends FragmentPagerAdapter
{
    private List<Fragment> frags = new ArrayList<>();
    private List<String> titles = new ArrayList<>();

    PageAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int pos) {
        return frags.get(pos);
    }

    @Override
    public int getCount() {
        return frags.size();
    }

    @Override
    public CharSequence getPageTitle(int pos)
    {
        return titles.get(pos);
    }

    void addFrag(Fragment frag, String title)
    {
        frags.add(frag);
        titles.add(title);
    }
}
