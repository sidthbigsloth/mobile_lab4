package com.example.sindre.lab4;

import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MessagesFragment extends Fragment {
    final String CHANNEL_ID = "Lab 4. ";
    public FirebaseFirestore db;

    ListView list;
    List<Message> messages;

    private Runnable runner;
    private Handler handler;
    private boolean newMessage;

    String lastUser;
    String lastMessage;

    public MessagesFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.fragment_messages, container, false);

        db = FirebaseFirestore.getInstance();
        list = rootView.findViewById(R.id.list);
        messages = getMessages();
        newMessage = false;

        handler = new Handler();

        runner = new Runnable() {
            @Override
            public void run() {
                new Notifier().execute();
                handler.postDelayed(runner, getRefresh());
            }
        };
        handler.post(runner);

        return rootView;
    }

    private ArrayList<Message> getMessages()
    {   //gets messages
        final ArrayList<Message> messageArrayList = new ArrayList<>();

        db.collection("messages").addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(QuerySnapshot messages, FirebaseFirestoreException e) {
                if(e != null)
                {
                    Log.d("MessagesFragment", "Listener failed", e);
                    return;
                }
                else
                    Log.d("MessagesFragment", "Listener success");

                messageArrayList.clear();
                for(DocumentSnapshot doc : messages)
                {
                    if(doc.get("user") != null && doc.get("message") != null && doc.get("date") != null)
                    {
                        Message message = new Message();
                        message.setUser(doc.get("user").toString());
                        message.setMessage(doc.get("message").toString());
                        message.setDate(doc.get("date").toString());
                        messageArrayList.add(message);

                    }
                }

                Collections.sort(messageArrayList, new Comparator<Message>() {
                    @Override
                    public int compare(Message message, Message t1)
                    {
                        if(message.getDate() == null || t1.getDate() == null)
                            return 0;
                        return t1.getDate().compareTo(message.getDate());
                    }
                });


                list.setAdapter(new MyAdapter(getContext(), messageArrayList));


                if(messageArrayList.size()>0)
                {
                    lastUser = messageArrayList.get(0).getUser();
                    lastMessage = messageArrayList.get(0).getMessage();
                    newMessage = true;
                }
            }
        });
        return messageArrayList;
    }

    public class Notifier extends AsyncTask <Void, Void, Exception>
    {   //notifies user if there is a new message
        Exception exception = null;

        @Override
        protected Exception doInBackground(Void... voids)
        {
            messages = getMessages();
            if(newMessage)
            {
                notification();
                newMessage = false;
            }

            return exception;
        }
    }

    public void notification()
    {   //builds notification
        Intent intent = new Intent(getContext(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(getActivity(), 0, intent, 0);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(getContext());

        NotificationCompat.Builder builder = new NotificationCompat.Builder(getContext(), CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_announcement_black_24dp)
                .setContentTitle(lastUser)
                .setContentText(lastMessage)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);

        int notifID = 1;
        notificationManager.notify(notifID, builder.build());
    }

    public int getRefresh()
    {   //gets desired refresh rate in milisecs
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        int refresh = prefs.getInt("refresh", 1);
        int refreshTime = 0;

        switch (refresh) {
            case 0:
                refreshTime = 1000 * 60 * 10;
                break;
            case 1:
                refreshTime = 1000 * 60 * 60;
                break;
            case 2:
                refreshTime = 1000 * 60 * 60 * 24;
                break;
        }

        return refreshTime;
    }
}
