package com.example.sindre.lab4;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class MyAdapter extends BaseAdapter {
    private ArrayList<Message> messages;
    private LayoutInflater inflater;
    MyAdapter(Context context, ArrayList<Message> items) {
        messages = items;
        inflater = LayoutInflater.from(context);
    }
    /*private view holder class*/
    private class ViewHolder {
        TextView txtUser;
        TextView txtMessage;
        TextView txtDate;
    }
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_item, null);
            holder = new ViewHolder();
            holder.txtUser = convertView.findViewById(R.id.user);
            holder.txtMessage = convertView.findViewById(R.id.message);
            holder.txtDate = convertView.findViewById(R.id.date);
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }/**/
        Message message = messages.get(position);
        holder.txtUser.setText(message.getUser());
        holder.txtMessage.setText(message.getMessage());
        holder.txtDate.setText(message.getDate());
        notifyDataSetChanged();

        return convertView;
    }
    @Override
    public int getCount() {
        return messages.size();
    }
    @Override
    public Object getItem(int position) {
        return messages.get(position);
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
}
